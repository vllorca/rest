const express = require('express')
const app = express()


let users = {
    '1': { id: 1, firstName: 'Vivien', lastName: 'Llorca' },
    '2': { id: 2, firstName: 'Tom', lastName: 'Gedusor' }
}

// MIDDLEWARES

app.use((request, response, next) => {
    console.log('url', request.url, 'method', request.method)
    next()
})

// body-parser json
app.use(express.json())

app.use('/api/users/:id', function (request, response, next) {
    if (request.method === 'GET') {
        response.send(users[request.params.id])
    } else if (request.method === 'PATCH'){
        let user = users[request.params.id]
        let data = request.body
        Object.assign(user, data) 
        response.send(user)
    } else if (request.method === 'DELETE'){
        delete users[request.params.id]
        // users[request.params.id] = undefined
        response.sendStatus(203)

    } else {
        next()
    }
})

app.use('/api/users', function (request, response, next) {
    if (request.method === 'GET') {
        response.send(Object.values(users))
    } else {
        next()
    }
})

app.use((request, response, next) => {
    response.status(404).send('<h1>Error</h1>')
})

app.listen(3000, function () {
    console.log("Server listening on port 3000")
})
